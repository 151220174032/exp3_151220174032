/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
#include <iostream>
using namespace std;

Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	
	this->r = r; 
}

void Circle::setR(int r)
{
	this->r = (double)r;
}

double Circle::getR() const{
	return r;
}

double Circle::calculateCircumference() const{
	return (PI * r * 2);
}

double Circle::calculateArea() const{
	return (PI * r * r);
}

void Circle::isItEqual(Circle c1) const
{
	if (equals(c1)) {
		cout << "Circles are equal.\n";
	}
	else {
		cout << "Circles are not equal.\n";
	}
}

bool Circle::equals(Circle c1) const
{
	return r==c1.getR();
}
